import 'package:flutter/material.dart';

class ColorPallete {
  static const primaryColor = Color(0xff1565c0);
  static const primaryDarkColor = Color(0xff607Cbf);
  static const underlineTextField = Color(0xffffffff);
  static const hintColor = Color(0xffe8eaf6);
}