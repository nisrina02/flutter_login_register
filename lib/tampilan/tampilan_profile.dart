import 'package:flutter/material.dart';

class TampilanProfile extends StatelessWidget {
  const TampilanProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF09141A),
      appBar: AppBar(
        title: Text("About"),
        // centerTitle: true,
        backgroundColor: Color(0xFF09141A),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 60, right: 20, left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Full Name :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Gender :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Birthday :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Horoscope :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Zodiac :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Height :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Weight :",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.33),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 30),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nisrina Izdihar Ardana Putri",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Female",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "01 November 2002",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Scorpio",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Scorpio",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "160 cm",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "50 Kg",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ),
      ),
    );
  }
}