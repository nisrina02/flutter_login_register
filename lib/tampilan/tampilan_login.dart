import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_login_register/constants.dart';
import 'package:flutter_login_register/tampilan/tampilan_home.dart';
import 'package:flutter_login_register/tampilan/tampilan_register.dart';
import 'package:flutter_login_register/tampilan/tampilan_profile.dart';
import 'package:http/http.dart' as http;

class TampilanLogin extends StatefulWidget {
  const TampilanLogin({super.key});

  @override
  State<TampilanLogin> createState() => _TampilanLoginState();
}

class _TampilanLoginState extends State<TampilanLogin> {
  bool isHidden = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 70, horizontal: 20),
        child: ListView(
          children: <Widget> [
            _titleDesc(),
            _textField(),
            _buildButton(context),
          ],
        ),
        decoration: BoxDecoration(
          gradient: RadialGradient(
            center: Alignment(-1.0, -1.0),
            colors: [
              Color.fromRGBO(31, 66, 71, 1),
              Color.fromRGBO(13, 29, 35, 1),
              Color.fromRGBO(9, 20, 26, 1),
            ],
          )
        ),
      ),
    );
  }

  Widget _titleDesc() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 16)
        ),
        Text(
          "Login",
          style: TextStyle(
            color: Colors.white,
            fontSize: 24,
            fontFamily: 'sans-serif',
            fontWeight: FontWeight.w700
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12)
        )
      ],
    );
  }

  Widget _textField() {
    return Column(
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 12)
        ),
        TextFormField(
          controller: emailController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Enter Username/Email",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4))
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
        ),
        Padding(
          padding: EdgeInsets.only(top: 20)
        ),
        TextFormField(
          controller: passwordController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Enter Password",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4)),
            suffixIcon: IconButton(
              onPressed: () {
                if (isHidden == true) {
                  isHidden = false;
                } else {
                  isHidden = true;
                }
                setState(() {
                  
                });
              }, 
              icon: Icon(Icons.remove_red_eye_sharp)
            )
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
          obscureText: isHidden,
        ),
      ],
    );
  }

  Widget _buildButton(BuildContext context) {
    return Column(
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 30)
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            // padding: EdgeInsets.all(12),
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)
            )
          ),
          child: Text(
            "Login",
            style: TextStyle(
              color: Colors.white70,
              fontSize: 16,
              fontWeight: FontWeight.w700
            ),
          ),
          onPressed: () async {
            var res = await http.get(
              Uri.parse('http://techtest.youapp.ai/api/login?email=${emailController.text}&password=${passwordController.text}'),
            );
            Map<String, dynamic> dataResponse = jsonDecode(res.body) as Map<String, dynamic>;
            if (dataResponse['message'] == 'success') {
              Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => TampilanHome())
                // context, MaterialPageRoute(builder: (_) => TampilanProfile())
              );
            }
          }
        ),
        Padding(
          padding: EdgeInsets.only(top: 10)
        ),
        TextButton(
          onPressed: () {
            Navigator.pushNamed(context, TampilanRegister.routeName);
          }, 
          child: Text(
            "No Account? Register here",
            style: TextStyle(
              color: Colors.white70
            ),
          )
        ),
      ],
    );
  }
}