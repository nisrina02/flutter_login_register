import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_login_register/constants.dart';
import 'package:http/http.dart' as http;

class TampilanRegister extends StatefulWidget {
  const TampilanRegister({super.key});
  static const routeName = "halamanRegister";

  @override
  State<TampilanRegister> createState() => _TampilanRegisterState();
}

class _TampilanRegisterState extends State<TampilanRegister> {

  bool isHidden = true;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPassController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: RadialGradient(
            center: Alignment(-1.0, -1.0),
            colors: [
              Color.fromRGBO(31, 66, 71, 1),
              Color.fromRGBO(13, 29, 35, 1),
              Color.fromRGBO(9, 20, 26, 1),
            ],
          )
        ),
        padding: EdgeInsets.symmetric(vertical: 70, horizontal: 20),
        child: ListView(
          children: <Widget> [
            _titleDesc(),
            _textField(),
            _buildButton(context)
          ],
        ),
      ),
    );
  }

  Widget _titleDesc() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 16)
        ),
        Text(
          "Register",
          style: TextStyle(
            color: Colors.white,
            fontSize: 24,
            fontFamily: 'sans-serif',
            fontWeight: FontWeight.w700
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12)
        )
      ],
    );
  }

  Widget _textField() {
    return Column(
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 12)
        ),
        TextFormField(
          controller: emailController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Enter Email",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4))
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
        ),
        Padding(
          padding: EdgeInsets.only(top: 12)
        ),
        TextFormField(
          controller: nameController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Create Username",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4))
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
        ),
        Padding(
          padding: EdgeInsets.only(top: 12)
        ),
        TextFormField(
          controller: passwordController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Create Password",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4)),
            suffixIcon: IconButton(
              onPressed: () {
                if (isHidden == true) {
                  isHidden = false;
                } else {
                  isHidden = true;
                }
                setState(() {
                  
                });
              }, 
              icon: Icon(Icons.remove_red_eye_sharp)
            )
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
          obscureText: isHidden,
        ),
        Padding(
          padding: EdgeInsets.only(top: 12)
        ),
        TextFormField(
          controller: confirmPassController,
          decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(255, 255, 255, 0.06),
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10)
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 1.5
              )
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.06),
                width: 3.0
              )
            ),
            hintText: "Confirm Password",
            hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4)),
            suffixIcon: IconButton(
              onPressed: () {
                if (isHidden == true) {
                  isHidden = false;
                } else {
                  isHidden = true;
                }
                setState(() {
                  
                });
              }, 
              icon: Icon(Icons.remove_red_eye_sharp)
            )
          ),
          style: TextStyle(color: Colors.white70),
          autofocus: false,
          obscureText: isHidden,
        ),
      ],
    );
  }

  Widget _buildButton(BuildContext context) {
    return Column(
      children: <Widget> [
        Padding(
          padding: EdgeInsets.only(top: 30)
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)
            )
          ),
          child: Text(
            "Register",
            style: TextStyle(
              color: Colors.white70,
              fontSize: 16,
              fontWeight: FontWeight.w700
            ),
          ),
          onPressed: () async {
            var res = await http.post(
              Uri.parse('http://techtest.youapp.ai/api/register'),
              body: {
                'email': emailController.text,
                'name': nameController.text,
                'password': passwordController.text
              }
            );
            Map<String, dynamic> dataResponse = jsonDecode(res.body) as Map<String, dynamic>;
            if (dataResponse['message'] == 'success') {
              Navigator.pushNamed(context, "/");
            }
          }
        ),
        Padding(
          padding: EdgeInsets.only(top: 10)
        ),
        TextButton(
          onPressed: () {
            Navigator.pushNamed(context, "/");
          }, 
          child: Text(
            "Have an Account? Login here",
            style: TextStyle(
              color: Colors.white70
            ),
          )
        )
      ],
    );
  }
}