import 'package:flutter/material.dart';
import 'package:flutter_login_register/tampilan/tampilan_register.dart';
import 'package:flutter_login_register/tampilan/tampilan_profile.dart';
import 'package:flutter_login_register/tampilan/tampilan_login.dart';
import 'package:flutter_login_register/tampilan/tampilan_home.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Login, Register Profile",
    initialRoute: '/',
    routes: {
      "/":(context) => TampilanLogin(),
      "p":(context) => TampilanProfile(),
      "/home":(context) => TampilanHome(),
      TampilanRegister.routeName: (context) => TampilanRegister(),
    },
  ));
}
